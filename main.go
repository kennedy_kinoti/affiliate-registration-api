package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
	"path/filepath"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
)

var router *chi.Mux
var db *sql.DB

// Affiliate details struct
type Affiliate struct {
	ID           int
	BusinessType string   `json:"business_type"`
	BusinessName string   `json:"business_name"`
	Industry     []string `json:"industry"`
	Phone        string   `json:"phone"`
	Email        string   `json:"email"`
	Password     string   `json:"password"`
	RcCode       string   `json:"rc_code,omitempty"`
	Terms        bool     `json:"terms"`
	IsDuplicate  bool     `json:"is_duplicate"`
}

// IndustryString to array
type IndustryString struct {
	Industry string `json:"industry"`
}

// EmailData struct
type EmailData struct {
	Name    string
	Subject string
	Body    string
	URL     string
}

var testTemplate *template.Template

func init() {
	router = chi.NewRouter()
	router.Use(middleware.Recoverer)

	var err error

	if err := godotenv.Load(); err != nil {
		log.Println("File .env not found, reading configuration from ENV")
	}

	dbName := os.Getenv("dbName")
	dbPass := os.Getenv("dbPass")
	dbHost := os.Getenv("dbHost")
	dbPort := os.Getenv("dbPort")

	dbSource := fmt.Sprintf("root:%s@tcp(%s:%s)/%s?charset=utf8", dbPass, dbHost, dbPort, dbName)
	db, err = sql.Open("mysql", dbSource)
	catch(err)
}

func routers() *chi.Mux {
	router.Get("/", ping)

	router.Get("/affiliate", AllAffiliates)
	router.Get("/affiliate/{id}", DetailAffiliate)
	router.Post("/affiliate/create", CreateAffiliate)
	// router.Get("/affiliate/validate/", IsDuplicate)

	return router
}

// server starting point
func ping(w http.ResponseWriter, r *http.Request) {
	respondwithJSON(w, http.StatusOK, map[string]string{"message": "Welcome to iPay Affiliate Registration API"})
}

//-------------- API ENDPOINT ------------------//

// AllAffiliates get all the affiliates
func AllAffiliates(w http.ResponseWriter, r *http.Request) {
	errors := []error{}
	payload := []Affiliate{}

	rows, err := db.Query("Select id, business_type, business_name, industry, phone, email, password, rc_code, terms From affiliate_registration")
	catch(err)

	defer rows.Close()

	for rows.Next() {
		data := Affiliate{}
		inda := IndustryString{}

		er := rows.Scan(
			&data.ID,
			&data.BusinessType,
			&data.BusinessName,
			&inda.Industry,
			&data.Phone,
			&data.Email,
			&data.Password,
			&data.RcCode,
			&data.Terms,
		)

		data.Industry = strings.Split(inda.Industry, ",")

		// fmt.Println(data)

		if er != nil {
			errors = append(errors, er)
		}
		payload = append(payload, data)
	}

	respondwithJSON(w, http.StatusOK, payload)
}

// DetailAffiliate get specific affiliate details
func DetailAffiliate(w http.ResponseWriter, r *http.Request) {
	payload := Affiliate{}
	inda := IndustryString{}

	id := chi.URLParam(r, "id")
	// fmt.Println(id)

	row := db.QueryRow("Select id, business_type, business_name, industry, phone, email, password, rc_code, terms From affiliate_registration where id=?", id)

	err := row.Scan(
		&payload.ID,
		&payload.BusinessType,
		&payload.BusinessName,
		&inda.Industry,
		&payload.Phone,
		&payload.Email,
		&payload.Password,
		&payload.RcCode,
		&payload.Terms,
	)

	payload.Industry = strings.Split(inda.Industry, ",")

	if err != nil {
		respondWithError(w, http.StatusNotFound, "no rows in result set")
		return
	}

	respondwithJSON(w, http.StatusOK, payload)
}

// DateStamp Logs folder
func LogFolder()string{
	// Log file name
	dt := time.Now()

	// log file path
	newpath := filepath.Join(".", "logs")
	
	os.Mkdir(newpath, os.ModePerm)

	return newpath+"/"+dt.Format("2006-01-02")+".log"
}

// CreateAffiliate create a new affiliate
func CreateAffiliate(w http.ResponseWriter, r *http.Request) {
	affiliate := Affiliate{}
	json.NewDecoder(r.Body).Decode(&affiliate)

	filename := LogFolder()

	f, err := os.OpenFile(filename, 
		os.O_RDWR | os.O_CREATE | os.O_APPEND, 0644)
	if err != nil {
		log.Fatalf("error opening log file: %v", err)
		log.Println( "error opening log file", err)
	}
	defer f.Close()

	log.SetOutput(f)

	industryString := strings.Join(affiliate.Industry, ", ")

	fmt.Printf("User doesn't exist... Creating ")
	log.Println( "Create Affiliate API:")

	go SendEmail(affiliate)

	query, err := db.Prepare("Insert affiliate_registration SET business_type=?, business_name=?, industry=?, phone=?,  email=?, password=?, rc_code=?, terms=?")
	log.Println( map[string]string{"error": "sample message", "status": "false"} )
	catch(err)

	_, er := query.Exec(affiliate.BusinessType, affiliate.BusinessName, industryString, affiliate.Phone, affiliate.Email, affiliate.Password, affiliate.RcCode, affiliate.Terms)
	catch(er)

	if er != nil {
		log.Fatal("Cannot run insert statement", er)
	}

	defer query.Close()

	respondwithJSON(w, http.StatusCreated, map[string]string{"message": "affiliate successfully created", "status": "true"})
	
	log.Println( map[string]string{"message": "affiliate successfully created", "status": "true"})

}

// SendEmail HTML to new Affiliate
func SendEmail(m Affiliate) {
	if err := godotenv.Load(); err != nil {
		log.Println("File .env not found, reading configuration from ENV")
	}

	nameData := m.BusinessName
	rcCodeData := m.RcCode
	emailData := m.Email
	passData := m.Password

	url := os.Getenv("emailURL")
	from := os.Getenv("emailFrom")
	emailName := os.Getenv("emailFromName")
	emailAPI := os.Getenv("emailAPIKey")

	postData := make(map[string]interface{})
	postData["from"] = from
	postData["fromName"] = emailName
	postData["apikey"] = emailAPI
	postData["subject"] = "iPay Affiliate, Karibu"
	postData["to"] = emailData
	postData["bodyHtml"] = `
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<title>Newsletter</title>
				<link rel="stylesheet" href="https://channels.intrepidprojects.co.ke/icons.css">
				<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
			</head>
			<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color: #eee;">
				<br>
				<div style="margin:0 auto !important;padding:20px; max-width: 500px;background-color: #fff;border-radius: 4px;">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tbody>
						<tr>
							<td bgcolor="#ffffff" align="center">
								<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:500px" class="m_-9170412488597135291wrapper">
									<tbody>
									<tr>
										<td align="center" valign="top" style="padding:15px 0 10px 0;font-size:11px;font-family:Helvetica,Arial,sans-serif" class="m_-9170412488597135291logo">
											<a target='_blank' href="https://ipayafrica.com" target="_blank">
												<div class="ipay_ipay" style="height:45px;background-position: center;"></div>
											</a>
										</td>
									</tr>
									</tbody>
								</table>
								<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:500px" class="m_-9170412488597135291responsive-table">
									<tbody>
									<tr>
										<td bgcolor="#ffffff" align="center">
											<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:500px;border-bottom:1px dotted #aaa" class="m_-9170412488597135291wrapper">
												<tbody>
												<tr>
													<td align="center" valign="top" style="padding:10px 0;font-size:18px;color:#333;font-family:Helvetica,Arial,sans-serif;text-transform:uppercase" class="m_-9170412488597135291logo">
														iPay Africa Affiliate
													</td>
												</tr>
												</tbody>
											</table>
											<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:500px" class="m_-9170412488597135291responsive-table">
												<tbody>
												<tr>
													<td>
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tbody>
															<tr>
																<td align="left" style="padding:20px 0 10px 0;text-align:center;font-size:14px;line-height:25px;font-family:Helvetica,Arial,sans-serif;color:#666666" class="m_-9170412488597135291padding-copy">Welcome and Thank you for registering for the iPay Affiliate Program.</td>
															</tr>
															<tr>
																<td align="left" style="padding:20px 0 30px 0;text-align:center;font-size:12px;line-height:20px;font-family:Helvetica,Arial,sans-serif;color:#666666" class="m_-9170412488597135291padding-copy"><small>Your account details are as below </small>.</td>
															</tr>
															
															</tbody>
														</table>
													</td>
												</tr>
												<tr>
													<td style="padding:10px 0 0 0;border-top:1px dotted #aaa">
														<table cellspacing="0" cellpadding="0" border="0" width="100%">
															<tbody>
															<tr>
																<td valign="top" style="padding:0" class="m_-9170412488597135291mobile-wrapper">
																	<table cellspacing="0" cellpadding="0"  style="width: 100%; padding: 5px;font-size: 14;font-family: Helvetica,Arial,sans-serif;color: #101010;">
																		<tr style="background-color: #eee;">
																		<td style="padding: 4px;">Name:</td>
																		<td style="padding: 4px;">` + nameData + `</td>
																		</tr>
																		<tr>
																		<td style="padding: 4px;"> RC Code:</td>
																		<td style="padding: 4px;">` + rcCodeData + `</td>
																		</tr>
																		<tr style="background-color: #eee;">
																		<td style="padding: 4px;">Email:</td>
																		<td style="padding: 4px;">` + emailData + `</td>
																		</tr>
																		<tr>
																		<td style="padding: 4px;">Password:</td>
																		<td style="padding: 4px;">` + passData + `</td>
																		</tr>
																	</table>
																	<br>
																</td>
															</tr>
															</tbody>
														</table>
													</td>
												</tr>
												<tr>
													<td>
													</td>
												</tr>
												<tr>
													<td>
													</td>
												</tr>
												<tr>
													<td>
													</td>
												</tr>
												<tr>
													<td style="border-bottom:1px dotted #aaa">
													</td>
												</tr>
												<tr>
													<td align="left" style="padding:20px 0 20px 0;text-align:center;font-size:12px;line-height:20px;font-family:Helvetica,Arial,sans-serif;color:#666666" class="m_-9170412488597135291padding-copy">
														Please change your password as soon as you login to your dashboard
													</td>
												</tr>
												</tbody>
											</table>
										</td>
									</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td bgcolor="#fff" align="center">
							</td>
						</tr>
						</tbody>
					</table>
				</div>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width:500px;margin: 0 auto;max-width: 500px;" class="m_-9170412488597135291responsive-table">
					<tbody>
					<tr>
						<td align="center" style="font-size:12px;line-height:18px;font-family:Helvetica,Arial,sans-serif;color:#666666; background-color: #eee;padding-top: 30px;padding-bottom: 30px;">
							For any queries access our FAQs: http://affiliates-faqs.ipayprojects.com/, alternatively you can contact us on: 
							<p>Skype: ipaysupport</p>
							<p>Telephone: +254 20 765 5555</p>
							<p>Email: info@ipayafrica.com , customerservice@ipayafrica.com </p>
						</td>
					</tr>
				</tbody>
				</table>
			</body>
		</html>
		`
	messages := make(chan string)

	go postCurl(messages, url, postData)
}

// postCurl
func postCurl(messages chan<- string, url string, postFields map[string]interface{}) {

	postFieldsString := ""

	for k, v := range postFields {
		if postFieldsString == "" {
			postFieldsString = k + "=" + fmt.Sprintf("%v", v)
		} else {
			postFieldsString = postFieldsString + "&" + k + "=" + fmt.Sprintf("%v", v)
		}
	}

	// log.Println(postFieldsString)

	payload := strings.NewReader(postFieldsString)
	req, _ := http.NewRequest("POST", url, payload)
	req.Header.Add("content-type", "application/x-www-form-urlencoded")
	req.Header.Add("cache-control", "no-cache")
	res, _ := http.DefaultClient.Do(req)

	if res != nil {
		defer res.Body.Close()
	}

	body, _ := ioutil.ReadAll(res.Body)

	response := string(body)
	// fmt.Println(response)
	messages <- response
}

func main() {
	routers()

	if err := godotenv.Load(); err != nil {
		log.Println("File .env not found, reading port configuration from ENV")
	}

	port := os.Getenv("port")

	http.ListenAndServe(":"+port, Logger())

}
